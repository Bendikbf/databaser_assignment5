<?php
include_once('Clubs.php');
include_once('SkierSeason.php');
include_once('Skier.php');

function domdoc($DBController, $doc)
{
  $xpath = new DOMXpath($doc);

  foreach($xpath->query('/SkierLogs/Skiers/Skier') as $skier)
  {
    $children = $skier->childNodes;
    $userName =$skier->getAttribute('userName');
    $firstName = $children->item(1)->nodeValue;
    $lastName = $children->item(3)->nodeValue;
    $birthYear = $children->item(5)->nodeValue;
    $DBController->addSkier(new Skier($userName, $firstName, $lastName, $birthYear));
  }

  foreach($xpath->query("/SkierLogs/Clubs/Club") as $club)
  {
    $children = $club->childNodes;
    $clubId =$club->getAttribute('id');
    $clubName = $children->item(1)->nodeValue;
    $city = $children->item(3)->nodeValue;
    $county = $children->item(5)->nodeValue;
    $DBController->addClub(new Clubs($clubId, $clubName, $city, $county));
  }

  foreach ($xpath->query('/SkierLogs/Season') as $skierSeason)
  {
    $season = $skierSeason->getAttribute('fallYear');

    foreach ($xpath->query("/SkierLogs/Season[@fallYear = $season]/Skiers") as $club)
    {
      if (!$club->getAttribute('clubId'))
      {
        $cid = NULL;

        foreach ($xpath->query("/SkierLogs/Season[@fallYear = $season]/Skiers[not(@*)]/Skier") as $username)
        {
          $uname = $username->getAttribute('userName');
          $totalDistance = 0;
          foreach ($xpath->query("/SkierLogs/Season[@fallYear = $season]/Skiers[not(@*)]/Skier[@userName = \"$uname\"]/Log/Entry/Distance") as $dist)
          {
            $totalDistance += $dist->nodeValue;
          }
          $DBController->addSkierSeason(new SkierSeason($uname, $season, $cid), $totalDistance);
        }
      }
      else
      {
        $cid = $club->getAttribute('clubId');

        foreach ($xpath->query("/SkierLogs/Season[@fallYear = $season]/Skiers[@clubId = \"$cid\"]/Skier") as $username)
        {
          $uname = $username->getAttribute('userName');
          $totalDistance = 0;

          foreach ($xpath->query("/SkierLogs/Season[@fallYear = $season]/Skiers[@clubId = \"$cid\"]/Skier[@userName = \"$uname\"]/Log/Entry/Distance") as $dist)
          {
            $totalDistance += $dist->nodeValue;
          }
          $DBController->addSkierSeason(new SkierSeason($uname, $season, $cid), $totalDistance);
        }
      }
    }
  }
}


 ?>
