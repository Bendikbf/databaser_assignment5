<?php

class Skier {
  public $userName;
  public $firstName;
  public $lastName;
  public $birthYear;

  public function __construct($userName, $firstName, $lastName, $birthYear){
    $this->userName = $userName;
    $this->firstName = $firstName;
    $this->lastName = $lastName;
    $this->birthYear = $birthYear;
  }
}

 ?>
