<?php

class Clubs
{
  public $clubId;
  public $clubName;
  public $city;
  public $county;

  public function __construct($clubId, $clubName, $city, $county)
  {
    $this->clubId = $clubId;
    $this->clubName = $clubName;
    $this->city = $city;
    $this->county = $county;
  }
}

 ?>
