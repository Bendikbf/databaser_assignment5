<?php
include_once('Skier.php');
include_once('SkierSeason.php');
include_once('Clubs.php');
include_once('DOMdoc.php');

$doc = new DOMDocument();
$doc->load('SkierLogs.xml');
$DBController = new XMLmodel();
domdoc($DBController, $doc);

class XMLmodel
{
  protected $db = null;

  public function __construct($db = null)
  {
    if ($db)
		{
			$this->db = $db;
		}
		else
		{
      //connection to db.
      $this->db = new PDO('mysql:host=localhost;dbname=assignment5;charset=utf8', 'root','',
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
  }


  public function addSkier($skier)
  {
    try
    {
      $stmt = $this->db->prepare("INSERT INTO skier(userName, firstName, lastName, birthYear)
      VALUES(:userName, :firstName, :lastName, :birthYear)");
      $stmt->bindValue(':userName', $skier->userName, PDO::PARAM_STR);
      $stmt->bindValue(':firstName', $skier->firstName, PDO::PARAM_STR);
      $stmt->bindValue(':lastName', $skier->lastName, PDO::PARAM_STR);
      $stmt->bindValue(':birthYear', $skier->birthYear, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch(PDOException $e){
      print_r($e->getMessage());
    }
  }


  public function addSkierSeason($SkierSeason, $totalDistance)
  {
    /*Something is wrong with the code here where $totalDistance is made NULL when this function is in use.
     I tested echoes in the constructor of SkierSeason as well as before and after the new SkierSeason() in DOMDoc,
     they all returned correct values. When I did the same here, they were suddenly all NULL

     Fixed by adding totalDistance as it's own parameter.*/
    try
    {
      $stmt = $this->db->prepare("INSERT INTO SkierSeason(uname, season, cid, totalDistance)
      VALUES(:uname, :season, :cid, :totalDistance)");
      $stmt->bindValue(':uname', $SkierSeason->uname, PDO::PARAM_STR);
      $stmt->bindValue(':season', $SkierSeason->season, PDO::PARAM_STR);
      $stmt->bindValue(':cid', $SkierSeason->cid, PDO::PARAM_STR);
      $stmt->bindValue(':totalDistance', $totalDistance, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch(PDOException $e){
      print_r($e->getMessage());
    }
  }


  public function addClub($Clubs)
  {
    try
    {
      $stmt = $this->db->prepare("INSERT INTO clubs(clubId, clubName, city, county)
      VALUES(:clubId, :clubName, :city, :county)");
      $stmt->bindValue(':clubId', $Clubs->clubId, PDO::PARAM_STR);
      $stmt->bindValue(':clubName', $Clubs->clubName, PDO::PARAM_STR);
      $stmt->bindValue(':city', $Clubs->city, PDO::PARAM_STR);
      $stmt->bindValue(':county', $Clubs->county, PDO::PARAM_STR);
      $stmt->execute();
    }
    catch(PDOException $e){
      print_r($e->getMessage());
    }
  }



}

?>
